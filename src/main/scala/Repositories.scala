import scala.concurrent.Future

trait Repo {
  def findUser(userId: String): Future[Option[User]]
  def findApps(): Future[Seq[App]]
  def findApp(id: String): Future[Option[App]]
  def findAppsByTeam(organizationId: String, teamId: String): Future[Seq[App]] = ???
  def findOrganizations(): Future[Seq[Organization]] = ???
  def findOrganization(id: String): Future[Option[Organization]] = Future.failed(AuthorizationException("Forbidden"))
  def findTeams(organizationId: String): Future[Seq[Team]] = ???
  def findUsersForOrga(organizationId: String): Future[Seq[User]] = ???
  def findUsersForTeam(organizationId: String, teamId: String): Future[Seq[User]] = ???
  def createApp(app: AppCreate): Future[App] = Future.successful(App(id = "0", name = app.name))
}

case class AuthorizationException(message: String) extends Exception(message)
