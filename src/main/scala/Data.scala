import java.time.ZonedDateTime

  // Json.obj(
  //     "id" -> user.id,
  //     "email" -> user.email,
  //     "name" -> user.customInfos.name,
  //     "surname" -> user.customInfos.surname,
  //     "officePhone" -> user.customInfos.officePhone,
  //     "mobilePhone" -> user.customInfos.mobilePhone,
  //     "description" -> user.customInfos.description,
  //     "appsLimit" -> user.appsLimit,
  //     "quotaWorkers" -> user.quotaWorkers,
  //     "isAdmin" -> user.isAdmin,
  //     "canCreateApp" -> user.canCreateApp,
  //     "isEnabled" -> user.isEnabled,
  //     "createdAt" -> user.createdAt,
  //     "lastConnection" -> user.lastConnection,
  //     "avatarUrl" -> User.avatarUrl(user.id, user.customInfos.avatarKey)
  //   )

case class User(
  id: String,
  email: String,
  name: Option[String],
  surname: Option[String],
  appsLimit: Int,
  quotaWorkers: Int,
  // ...
  createdAt: ZonedDateTime,
  avatarUrl: String
)

case class App(
  id: String,
  name: String
)

case class Organization(
  id: String
)

case class Team(
  id: String,
  organizationId: String
)

case class AppCreate(
  name: String,
  stack: Option[String]
)
