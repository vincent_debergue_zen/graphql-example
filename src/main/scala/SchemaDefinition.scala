import scala.concurrent.Future

import sangria.schema._
import sangria.macros.derive._

import SchemaUtils._

object SchemaDefinition {
  val UserType = deriveObjectType[Unit, User](
    ObjectTypeDescription("A nic user")
  )

  val AppType = deriveObjectType[Unit, App](
    ObjectTypeDescription("A nic application")
  )

  val TeamType = deriveObjectType[Repo, Team](
    ObjectTypeDescription("A team which is part of a team"),
    AddFields(
      Field("users", ListType(UserType),
        resolve = ctx => ctx.ctx.findUsersForTeam(ctx.value.organizationId, ctx.value.id)
      )
    )
  )

  val OrganizationType = deriveObjectType[Repo, Organization](
    ObjectTypeDescription("An Organization"),
    AddFields(
      Field("teams", ListType(TeamType),
        resolve = ctx => ctx.ctx.findTeams(ctx.value.id)
      ),
      Field("users", ListType(UserType),
        resolve = ctx => ctx.ctx.findUsersForOrga(ctx.value.id)
      )
    )
  )

  val IDArgument = Argument("id", StringType, description = "id of the resource")
  val organizationId = Argument("organizationId", StringType, description = "id of the origanization")
  val teamId = Argument("teamId", StringType, description = "id of the team")

  val QueryType = ObjectType("NicQuery", fields[Repo, Unit](
    Field("user", OptionType(UserType),
      arguments = IDArgument :: Nil,
      resolve = ctx => ctx.ctx.findUser(ctx.arg(IDArgument))
    ),
    Field("apps", ListType(AppType),
      resolve = ctx => ctx.ctx.findApps()
    ),
    Field("app", OptionType(AppType),
      arguments = IDArgument :: Nil,
      resolve = ctx => ctx.ctx.findApp(ctx.arg(IDArgument))
    ),
    Field("appsByOrgaAndTeam", ListType(AppType),
      arguments = organizationId :: teamId :: Nil,
      resolve = ctx => ctx.ctx.findAppsByTeam(ctx.arg(organizationId), ctx.arg(teamId))
    ),
    Field("organizations", ListType(OrganizationType),
      resolve = ctx => ctx.ctx.findOrganizations()
    ),
    Field("organization", OptionType(OrganizationType),
      arguments = IDArgument :: Nil,
      resolve = ctx => ctx.ctx.findOrganization(ctx.arg(IDArgument))
    )
  ))

  val AppCreateType = deriveInputObjectType[AppCreate](
    InputObjectTypeDescription("An app to be created")
  )
  import sangria.marshalling._
  // could be avoided if we had a `Format[AppCreate]`
  implicit val appCreateFromInput = new FromInput[AppCreate] {
    val marshaller = CoercedScalaResultMarshaller.default
    def fromResult(node: marshaller.Node) = {
      val ad = node.asInstanceOf[Map[String, Any]]
      AppCreate(
        name = ad("name").asInstanceOf[String],
        stack = ad.get("stack").flatMap(_.asInstanceOf[Option[String]])
      )
    }
  }

  val AppCreateArg = Argument("app", AppCreateType)

  val MutationType = ObjectType("NicMutation", fields[Repo, Unit](
    Field("createApp", OptionType(AppType),
      arguments = AppCreateArg :: Nil,
      resolve = ctx => ctx.ctx.createApp(ctx.arg(AppCreateArg))
    )
  ))

  val NICSchema = Schema(query = QueryType, mutation = Some(MutationType))
}

