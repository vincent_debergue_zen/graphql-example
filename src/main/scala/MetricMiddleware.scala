import sangria.execution._

import scala.collection.mutable.{ Map => MutableMap }
import sangria.schema.{Action, Context}

case class State(data: MutableMap[String, List[Long]], start: Long)

class MetricMiddleware extends Middleware[Any] with MiddlewareAfterField[Any] with MiddlewareErrorField[Any] {
  type QueryVal = State
  type FieldVal = Long

  def beforeQuery(context: MiddlewareQueryContext[Any, _, _]): QueryVal = State(MutableMap(), System.currentTimeMillis())
  def afterQuery(queryVal: QueryVal, context: MiddlewareQueryContext[Any, _, _]) =
    reportQueryMetrics(queryVal)

  def beforeField(queryVal: QueryVal, mctx: MiddlewareQueryContext[Any, _, _], ctx: Context[Any, _]) =
    continue(System.currentTimeMillis())

  def afterField(queryVal: QueryVal, fieldVal: FieldVal, value: Any, mctx: MiddlewareQueryContext[Any, _, _], ctx: Context[Any, _]) = {
    updateStateWithField(queryVal, fieldVal, ctx)
    None
  }

  def fieldError(queryVal: QueryVal, fieldVal: FieldVal, error: Throwable, mctx: MiddlewareQueryContext[Any, _, _], ctx: Context[Any, _]) = {
    updateStateWithField(queryVal, fieldVal, ctx)
    val errors = queryVal.data.getOrElse("ERROR", Nil)
    queryVal.data.update("ERROR", errors :+ 1L)
  }

  def updateStateWithField(queryVal: QueryVal, fieldVal: FieldVal, ctx: Context[Any, _]) {
    val key = ctx.parentType.name + "." + ctx.field.name
    val args = ctx.args.raw
    val key2 = if (args.isEmpty) key else (s"$key $args")
    val list = queryVal.data.getOrElse(key2, Nil)

    queryVal.data.update(key2, list :+ (System.currentTimeMillis() - fieldVal))
  }

  def reportQueryMetrics(queryVal: QueryVal) {
    val msg = queryVal.data.map { case (k, v) =>
      if (k != "ERROR") s"  $k took $v ms" else s"  Error count = ${v.sum}"
    }.mkString("\n")

    println(s"""----- Metrics  ----
    | Total = ${System.currentTimeMillis() - queryVal.start} ms
    | $msg
    |-----""".stripMargin)
  }
}
