import java.time.ZonedDateTime
import scala.util.{Try, Success, Failure}

import sangria.ast
import sangria.schema._
import sangria.validation._

object SchemaUtils {
  case object DateCoercionViolation extends ValueCoercionViolation("ZonedDateTime value expected")
  def parseZonedDateTime(s: String) = Try(ZonedDateTime.parse(s)) match {
    case Success(date) ⇒ Right(date)
    case Failure(_) ⇒ Left(DateCoercionViolation)
  }
  implicit val ZonedDateTimeType = ScalarType[ZonedDateTime]("ZonedDateTime",
    description = Some(
      "ISO 8601 Date Time"
    ),
    coerceOutput = (d, caps) => d.toString,
    coerceUserInput = {
      case s: String ⇒ parseZonedDateTime(s)
      case _ ⇒ Left(DateCoercionViolation)
    },
    coerceInput = {
      case ast.StringValue(s, _, _) ⇒ parseZonedDateTime(s)
      case _ ⇒ Left(DateCoercionViolation)
    })
}
