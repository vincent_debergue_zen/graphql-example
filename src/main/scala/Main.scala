import sangria.parser.QueryParser
import sangria.execution._
import sangria.renderer.SchemaRenderer

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util._

object Main {
  def main(args: Array[String]): Unit = {
    println("starting...")

    println("------ Schema --------")
    println(SchemaRenderer.renderSchema(SchemaDefinition.NICSchema))
    println("--------------")

    executeQuery("""query MyQuery {
     user(id: "123") {
      id
      email
      name
     }
     apps {
      name
     }
    }""")
    Thread.sleep(2000)

    executeQuery("""query {
      toto {
        name
      }
    }""")
    Thread.sleep(2000)

    executeQuery("""query {
      organizations {
        id
      }
    }""")
    Thread.sleep(2000)

    executeQuery("""query {
      organization(id: "2") {
        id
      }
    }
    """)
    Thread.sleep(2000)

    executeQuery("""mutation CreateMyApp {
      createApp(app: {name: "app-name"}) {
        id
        name
      }
    }
    """)
  }

  def executeQuery(query: String) = {
    val userToken = "t0ken"
    QueryParser.parse(query) match {
      case Success(queryAst) =>
        val repo = new AuthenticatedRepo(userToken)
        val metricMiddleware = new MetricMiddleware()
        println("executing query ...")
        Executor.execute(
          SchemaDefinition.NICSchema,
          queryAst,
          userContext = repo,
          middleware = metricMiddleware :: Nil,
          exceptionHandler = errorHandler
        ).map { res =>
            println("res", res)
          }.recover {
            case error: QueryAnalysisError => println("qae", error.resolveError)
            case error: ErrorWithResolver => println("ewr", error.resolveError)
            case err => println("other", err)
          }
      case Failure(err) => println("parse err", err)
    }
  }

  val errorHandler: Executor.ExceptionHandler = {
    case (m, AuthorizationException(msg)) => HandledException(msg)
    case (m, err) =>
      println("got error", err)
      HandledException("oops")
  }

  import scala.concurrent.Future
  import java.time.ZonedDateTime

  class AuthenticatedRepo(userToken: String) extends Repo {
    def defer[T](name: String)(f: => Future[T]): Future[T] = Future {
        println(s"Defering $name")
        Thread.sleep(1000)
      }.flatMap { _ =>
        println(s"Done defering $name")
        f
      }
    def findUser(userId: String) = defer("findUser") {
      println(s"finding user with token $userToken")
      Future.successful(Some(
        User(
          id = userId,
          email = "email@tto.com",
          name = None,
          surname = None,
          appsLimit = 0,
          quotaWorkers = 0,
          createdAt = ZonedDateTime.now,
          avatarUrl = "avatarUrl"
        )
      ))
    }

    val app1 = App("1", "app1")
    val app2 = App("2", "app2")
    def findApps(): Future[Seq[App]] = defer("findApps"){ Future.successful(Seq(app1, app2)) }
    def findApp(id: String): Future[Option[App]] = Future.successful(Some(app1))
  }
}
