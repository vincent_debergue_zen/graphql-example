name := """nestincloud-graphql"""

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "org.sangria-graphql" %% "sangria" % "0.7.3"
)

